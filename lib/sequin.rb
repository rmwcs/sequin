#!/usr/bin/env ruby
require 'pp'
require "thor"
require 'fileutils'
require 'yaml'
require  File.expand_path(File.dirname(__FILE__)  + '/listing')
require  File.expand_path(File.dirname(__FILE__)  + '/membership')

module Sequin

  class Seq < Thor


    no_tasks do
      # Some typical regular expressions for use by seq:match.
      # When seq:generate config is called, this hash is dumped to a yaml file
      def regex_hash
        h = 
        {'project1' =>  
          {'dpx' => '^[0-9]{3}_[0-9]{3}_s3d_(rt|lf)_r[0-9]{2}\.[0-9]{4}\.dpx$',
          'jpg' => '^[0-9]{3}_[0-9]{3}_s3d_r[0-9]{2}\.[0-9]{4}\.jpg$',
          'mcx' => '^[0-9]{3}_[0-9]{3}_ver[0-9]{2,3}\.[0-9]{4}\.xml$'},
          'project2' =>  
          { 'dpx' => '^[a-z]{2}_[0-9]{3}_[0-9]{4}_(L|R|C)\.[0-9]{4,5}\.dpx$',
          'jpg' => '^[a-z]{2}_[0-9]{3}_[0-9]{4}_(L|R|C)\.[0-9]{4,5}\.jpg$',
          'mov' => '^[a-z]{2}_h264_[0-9]{3,4}x[0-9]{3,4}_(film|pal|ntsc)\.mov$' }
        }
      end

      def regex_comments
        <<-COMMENTS
        #------------------------------------------------------------------------------------
        #
        # ~/.seq.yml 
        #
        # The regular expressions in this file represent naming conventions for various 
        # projects and file_types. By matching filenames against these regexes, you can 
        # quickly determine if they conform to the desired naming convention.
        #
        # The regexes are accessed on the command line using the hash keys separated by
        # ":". i.e. seq match <files> --regex project1:dpx
        #
        # For regex newbies, here are some disections:
        # ^[a-z]+_[0-9]{3}_s3d_(rt|lf)_r[0,1][0-9]\.[0-9]{4}\.dpx$
        # ^           : the next character must be at the start of the string
        # [a-z]+_     : one or more lower case letters followed by an underscore
        # [0-9]{3}_   : exactly 3 numbers
        # _s3d_       : "_s3d_"  
        # (rt|lf)     : "rt" or "lf"
        # r[0,1][0-9] : "r" followed by a 2 digit number between 00 and 19
        # \.          : "." It must be escaped
        # [0-9]{4}    : 4 padded number
        # \.dpx       : ".dpx" extension. 
        # $           : The preceding character must be the last character. "x" in this case.
        #------------------------------------------------------------------------------------

        COMMENTS
      end

    end


    # When the user types seq and gives no task, we want to show
    # the default help with our own mods added. 
    default_task :usage

    desc "usage", "Print the usage banner"
    def usage
      help  # default Task descriptions

      puts <<-BANNER
      ------------------------------------------------------------------------------------
      Seq is a collection of commands to help deal with sequences on disk
      
      More help here
      
      BANNER

    end







    # map "i" => :inspect   
    # desc "inspect [FILES]", "inspect raw sequence hash for debugging purposes"
    # def inspect(*files)
    #   files = Dir.new(".").entries if files.count == 0 
    #   l = Sequin::Listing.new(files)
    #   l.inspect
    # end


    map "l" => :list   
    desc "list [FILES]", "list files as sequences"
    method_option :verbose,:type => :numeric, :default => 1, :aliases => "-v", :desc => "0 => dont mention missing frames, 1 => show missing frame count, 2 => list missing frame numbers"
    def list(*files)
      unless files.count == 0 
        l = Sequin::Listing.new(files)
        l.list(options[:verbose])
      else
        msg = "No files."
        puts msg.red
        help :list
        return
      end
    end




    map "g" => :generate
    desc "generate [NAME]", "Generate a seq config file containing regular expression definitions used in the seq:match command. Currently, NAME must be config"
    method_option :force, :type => :boolean, :aliases => "-f", :desc => "Overwrite the file if it exists.", :default => false

    def generate(name)
      if name.downcase == "config"
        yml = File.join(ENV['HOME'],".seq.yml")
        FileUtils.rm(yml) if options[:force] && File.exists?(yml)
        if File.exists?(yml)
          msg = "Skipping write of #{yml} because it exists. Use --force to remove it."
          puts msg.red
          help :generate
          return
        else
          puts "Generating #{yml}"
          File.open(yml , 'w') do |f|
            f.write(regex_comments)
            YAML.dump(regex_hash, f)
          end
        end
      else
        msg = "Error: #{name} is not recognized."
        puts msg.red
        help :generate
        return
      end
    end

    map "c" => :check   
    desc "check [SIGNATURE]", "Check the existence of files defined by a sequence signature."
    method_option :show_missing, :type => :boolean, :aliases => "-m", :desc => "List files from the signature that are missing on disk"
    method_option :show_extra, :type => :boolean, :aliases => "-e", :desc => "List files that are on disk but not in the signature"
    method_option :show_found, :type => :boolean, :aliases => "-f", :desc => "List files from the signature that are on disk"
    method_option :show_all, :type => :boolean, :aliases => "-a", :desc => "List the union of all files on disk and in the signature."
    def check(*args)
      signature = args.join(" ")

      chunks = signature.split(/\[|\]/)
      if chunks.count < 2
        display_str = (signature.length > 60) ? "#{signature[0, 60]}..." : signature
        msg = "Error: #{display_str} is not a valid sequence signature."
        puts msg.red
        help :check
        return
      end

      fn_template = chunks[0].strip  
      numbers = chunks[1].split(/([0-9]+)/) 

      if numbers.count < 4
        display_str = (signature.length > 60) ? "#{signature[0, 60]}..." : signature
        msg = "Error: #{display_str} is not a valid sequence signature. Should have [<start> to <end> [by <by_frame>]]. "
        puts msg.red
        help :check
        return
      end


      match = /#+/.match(fn_template)
      if match.nil?  
        msg = "Error: #{fn_template} has no # characters so does not represent a sequence."
        puts msg.red
        help :check
        return
      end

      range_spec=Hash.new
      range_spec[:padding] = match[0].length
      range_spec[:start_frame] = numbers[1].to_i
      range_spec[:end_frame] = numbers[3].to_i
      range_spec[:by_frame] = numbers[5].to_i || 1

      if range_spec[:start_frame].nil? || range_spec[:end_frame].nil? || ( range_spec[:start_frame] >= range_spec[:end_frame] )    
        msg = "Error: start_frame #{start_frame} must be less than end_frame #{end_frame}."
        puts msg.red
        help :check
        return
      end

      # range_spec.merge!(options)
      membership =Sequin::Membership.new(fn_template, range_spec) 
      
      membership.output(options)

    end




    map "m" => :match   
    desc "match [FILES]", "Match filename naming conventions using a regular expression."
    method_option :regex, :type => :string, :aliases => "-r", :required => true, :desc => "Specify a regular expression literally, or by reference to one listed in ~/.seq.yml"
    method_option :show_matched, :type => :boolean, :aliases => "-m", :desc => "List files that matched the regex in green"
    method_option :show_failed, :type => :boolean, :aliases => "-f", :desc => "List files that failed to match the regex in red"
    method_option :show_all, :type => :boolean, :aliases => "-a", :desc => "List all the files in order with color coding."
    def match(*files)
      keys =  options[:regex].split(":")
      if keys.count != 2
        the_regex = options[:regex]
      else

        begin
          yml = File.join(ENV['HOME'],".seq.yml")
          regexp_hash = YAML::load_file(yml)
        rescue
          msg = "Error: opening file #{yml} - please check the file exists and is valid YAML.\nIf it doesn't exist, run: seq generate config."
          puts msg.red
          help :match
          return
        end

        unless regexp_hash.has_key?(keys[0]) 
          msg = "Error: #{keys[0]} not in #{yml} - please check the file is valid YAML and has a top level key: #{keys[0]}."
          puts msg.red
          help :match
          return
        end

        unless regexp_hash[keys[0]].has_key?(keys[1]) 
          msg = "Error: #{keys[1]} not in #{yml} - please check the file is valid YAML and has a second level key: #{keys[1]}."
          puts msg.red
          help :match
          return
        end

        the_regex = regexp_hash[keys[0]][keys[1]]

      end

      unless the_regex.nil?
        l = Sequin::Listing.new(files)
        l.match(the_regex, options) 
      else
        msg = "Error: No valid regular expression."
        puts msg.red
        help :match
        return
      end

    end

  end


end