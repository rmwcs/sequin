#!/usr/bin/env ruby
require 'pp'
require 'fileutils'

# add colors to String class
class String
  def colorize(code)
  "\e[0;#{code.to_s}m#{self}\e[0m"
    
    #  self
  end

  # I tried to put "define_method" in a loop but it was having none of it
  # Please accept my humble apologies
  def red; self.colorize(31);end  
  def green; self.colorize(32);end  
  def yellow; self.colorize(33);end  
  def blue; self.colorize(34);end  
  def purple; self.colorize(35);end  
  def cyan; self.colorize(36);end  
end



module Sequin

  class Sequin::Listing

    # The goal of this sequence listing algorithm is to find sequences without the need
    # for the user to specify where the numbers are in the filenames, or what characters
    # delimit the numbers. This can be tricky because a filename may contain many parts 
    # containing numbers. Here's a real world example: 004_055_s3d_lf_r04.0001.dpx 
    # How can we know which number part correctly represents the sequence? 
    # It could be any of the following
    # ###_055_s3d_lf_r04.0001.dpx
    # 004_###_s3d_lf_r04.0001.dpx
    # 004_055_s#d_lf_r04.0001.dpx
    # 004_055_s3d_lf_r##.0001.dpx
    # 004_055_s3d_lf_r04.####.dpx
    #
    # The strategy we employ is to use the number part that causes the file to be in the largest sequence.

    # Consider the files:
    # file.04.0001.ext
    # file.04.0002.ext
    # file.04.0003.ext
    # file.05.0001.ext
    # file.05.0002.ext
    # file.05.0003.ext
    #
    # There are 5 possible sequences here. Each file could be in 2 of them, one containing 2 files, one containing 3.
    # file.04.0001.ext could be in file.##.0001.ext or file.04.####.ext
    # There are more files in file.04.####.ext than file.##.0001.ext, so we use that sequence for this file.



    def initialize(files)
      @sequences = Hash.new  

      # make a hash containing a signature key for all possible sequences
      all_seqs = all_possible_sequences files

      # now select from that hash the best sequences according
      # to the strategy outlined above
      select_best_sequences(all_seqs, files)

      # we need to make a note of the padding because
      # we will be converting the number part to integers
      each_sequence { |k,v| v[:padding] = v[:numbers].min.length } 

      # convert the number part to integers and sort them
      # so we can figure out the range, by_frame, and missing frames
      each_sequence { |k,v| v[:numbers].collect! {|el| el.to_i} } 
      each_sequence { |k,v| v[:numbers].sort! } 

      # find the by_frame so that we can figure out which
      # frames are missing, if any. If we don't get 
      # the by_frame for a sequence that is by 2 for example, we would
      # think that every other frame was missing.
      each_sequence {|k,v| v[:by] = calc_by_frame v[:numbers]}

      # now we know the by frame, we can find out which frames 
      # are missing because the user will want to know this. 
      each_sequence do |k,v| 
        range =  v[:numbers].min..v[:numbers].max
        v[:missing] = []
        range.step(v[:by]) do |val| 
          v[:missing]  << val unless  v[:numbers].include? val
        end    
      end

      # we no longer need the filenames. We have enough info to
      # recreate them if necessary
      each_sequence {|k,v| v.delete(:names) }

      # here we change the hash sequence keys (which are arrays). We make the number slot
      # represent the padding. This is so we can create a sequence key 
      # by simply calling join on the array elements
      each_sequence { |k,v|  k[v[:numbers_index]] = ("#" *v[:padding]) } 
    end


    # for debugging
    def inspect
      pp @sequences
    end



    # a sequence string describes the sequence like so.
    # filename [ <start> to <end> by <by_frame> ] - missing [ <missing frames> ]
    # This is what gets printed out in a listing, and can also be used as input 
    # to other seq commands such as seq pad, seq renumber, seq substitute
    # The missing frames section is optional. When a sequence_signature is used as input
    # the missing frames will be determined by seq internally. 
    def sequence_signature(k,v,verbose=1)
      range_str= "[#{v[:numbers].min} to #{v[:numbers].max} by #{v[:by]}]"
      unless v[:missing].empty? 
        range_str = "#{range_str} - missing: [ #{v[:missing].count} frames ]" if  verbose == 1
        range_str = "#{range_str} - missing: [ #{v[:missing].join(",")} ]"if  verbose == 2
        range_str = range_str.red
      else
        range_str = range_str.green
      end
      str = "#{k.join()} #{range_str}"
    end



    # simple listing of single files and sequences
    def list(verbose=1)
      each_single {|f| puts f}
      
      each_sequence do |k,v|
        str = sequence_signature(k,v,verbose)
        puts str
      end
    end


    # match a sequence against a regex.
    # create an array where 
    # each element is a sub array of the form    
    # ["filename", <:matched|:failed>, frame_number]
    #
    # The sequence is more managable as a single 
    # array as it can be sorted.
    def self.match_matrix(regex, key , seq)
      prefix, suffix = key.join().split(/#+/)
      matrix = [] 
      seq[:numbers].each do |n|
        fn =  [prefix , ("%0#{seq[:padding]}d" % n)  , suffix].join()
        unless regex.match(File.basename(fn)).nil?
          matrix << [fn,:matched,n]
        else
          matrix << [fn,:failed,n]
        end
      end
      matrix.sort! { |a,b|  a[2] <=> b[2] }
    end





    # make a one line message describing the result of this 
    # matching a regex against all files in a sequence. Most of
    # the info we need is in the fn_matrix array, but we also
    # need the sequence key/value pair to create the signature.
    def assessment_message(fn_matrix,k,v)
      matched = fn_matrix.count { |el| el[1] == :matched }
      failed = fn_matrix.count - matched

      msg = "#{matched} MATCHED"
      msg = "#{msg}, #{failed} FAILED" if failed > 0
      msg = "(#{msg})"
      msg = "#{msg} / #{fn_matrix.count} files" if failed > 0 &&  matched > 0

      if failed == 0
        msg = msg.green
      elsif matched == 0    
        msg = msg.red
      else
        msg = msg.yellow
      end

      fn = sequence_signature(k,v,0)
      message = "#{fn} #{msg}"
    end
    
    

    # this proc is called from the main script. Matches a regex against 
    # all single files and all sequences in the @sequences object and outputs
    # information according to verbosity options
    def match(the_regex, options ={})
      re = Regexp.new(the_regex)
      puts "Testing against regular expression: /#{the_regex}/".cyan

      each_single do |f| 
        msg = re.match(File.basename(f)).nil? ? 'FAILED'.red : 'PASSED'.green
        puts "#{f} #{msg}"
      end

      each_sequence do |k,v| 
        fn_matrix = Sequin::Listing.match_matrix(re, k,v)
        puts "#{assessment_message(fn_matrix,k,v)}"
        unless  options["show_all"].nil? &&  options["show_matched"].nil? &&  options["show_failed"].nil?
          fn_matrix.each do  |el|
            key =  "show_#{el[1].to_s}" 
            puts el[0].send(Sequin::Listing.alert_colors[el[1]]) unless options[key].nil? && options["show_all"].nil?
          end
        end
      end

    end


    # takes a block and iterates through sequence 
    # keys, ignoring single files
    def each_sequence
      @sequences.each do |k,v|
        yield(k,v)  unless k == :single   
      end
    end

    # takes a block and iterates through single 
    # files, ignoring . and .. and  sequences
    def each_single
      @sequences.each do |k,v|
        if k == :single   
          v.each  { |f|  yield(f) unless( f =="." || f == "..") }
        end
      end
    end


    private


    def all_possible_sequences(files)
      h = Hash.new
      files.each do |f|

        # an array containing the number parts in the odd slots: [1] [3] etc.
        parts = f.split(/([0-9]+)/) 

        # If there are an even number of parts, half of them are numbers
        # If there are an odd number of parts, there are more not-numbers than numbers
        # By dividing by 2 and relying on integer rounding, we get the right answer in both cases 
        number_count = parts.count / 2 

        # For every number position, make our hash key from the remaining parts
        # and make the hash value another hash containing the filenames, numbers, and other info
        (0...number_count).each do |i|
          number_part_index = (i*2)+1
          key =[]
          (0...parts.count).each do |part_index|
            key  <<  (part_index == number_part_index  ? "#" : parts[part_index] )
          end

          h[key] ||= {}  
          (h[key][:names] ||= []) <<  f
          (h[key][:numbers] ||= []) <<  parts[number_part_index]   
          h[key][:numbers_index] = number_part_index   
        end 
      end
      h
    end


    # Find the best sequence that each file belongs to.
    # As mentioned above, the best sequence is the one with 
    # the most files out of the set of all sequences that 
    # this file is in.
    def select_best_sequences(all_seqs, files)
      files.each do |f|
        most_frames = 0
        best_hash = {}
        best_key = nil

        all_seqs.each do |k,v|
          curr_frames = all_seqs[k][:numbers].count
          if curr_frames > most_frames
            if all_seqs[k][:names].include?(f)
              best_hash = all_seqs[k]
              best_key = k
              most_frames = curr_frames
            end
          end
        end
        if best_key.nil? || most_frames < 2
          ( @sequences[:single] ||= [] ) << f
        else 
          @sequences[best_key] = best_hash
        end
      end
    end

    # to determine the by_frame, we calculate the greatest common factor of
    # all the gaps between successive frames. If the gap gets to 1, then break
    # because we don't consider fractional frame numbers. 1 is the smallest gap allowed.
    # Note: with this method, we can still know the by_frame even if some frames
    # are missing 
    def calc_by_frame(frames)
      gcf = frames.max - frames.min
      last = frames.min
      frames.each do |n|
        if n !=  frames.min
          new_gcf = greatest_common_factor(gcf, n - last)
          gcf = new_gcf if new_gcf < gcf
          break if gcf == 1
          last = n
        end
      end
      gcf
    end


    # recursive GCF
    def greatest_common_factor(n1, n2) 
      return ( n1 % n2==0 ) ? n2 : greatest_common_factor(n2, n1 % n2 ) 
    end 

    def self.alert_colors
      {  :matched =>"green",  :failed =>"red"  }
    end



  end


end
