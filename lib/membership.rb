

module Sequin

  
  class Membership
    
    # Membership
    # In order to give information about files in relation to
    # the sequence signature, we need to collect all the filenames 
    # the user asks for, and all the filenames on disk that 
    # match the sequence mask (the filename part of the sequence signature). 
    # Thewn we can determine whether the file was (found), (misssing), 
    # or (extra) i.e. existing but not asked for
    #
    # Each element in the array returned by this method is a 
    # 3 element sub array that has the following form:
    # ["filename", <:found|:missing|:extra>, frame_number]
    # 
    # It is sorted on the frame_number, which is an integer
    def initialize(fn_template, options={} )

      @fn_template = fn_template
      @fn_matrix = Dir.glob(fn_template.gsub(/#+/, "*")).to_a
      prefix, suffix = @fn_template.split(/#+/)

      @fn_matrix.collect! { |f| [f, :extra] }

      range =  options[:start_frame]..options[:end_frame]
      range.step(options[:by_frame] || 1) do |val| 

        filename = fn_template.gsub(/#+/,  ("%0#{options[:padding]}d" % val) )
        
        el = @fn_matrix.find {|e| e[0] == filename }
        if el.nil?
          @fn_matrix << [filename , :missing ]
        else
          el[1] = :found
        end
      end
      @fn_matrix.each  { |el|   el[2] = el[0].sub(prefix,'').sub(suffix,'').to_i }
      @fn_matrix.sort! { |a,b|  a[2] <=> b[2] }
    end

    # generate a message from the filename matrix - something like this:
    # ../test/a/006_057_s3d_lf_r06.#.dpx [found: 13] [missing: 3] [extra: 17]
    # where the found missing and extra sections are color coded according
    # to whether there were files present with that attribute
    def assessment_message
      message = @fn_template
      [:found, :missing, :extra].each do |t|
       num_present = @fn_matrix.count { |el| el[1] == t }
        m = "[#{t.to_s}: #{num_present}]" 
        m = (num_present > 0) ? m.send(Sequin::Membership.alert_colors[t][:present]) :  m.send(Sequin::Membership.alert_colors[t][:absent]) 
        message = "#{message} #{m}"
      end
      message
    end

    # output a brief assessment message and optionally list files
    # that are found, missing, and/or extra
    def output(options={})
      puts "#{assessment_message}"
      @fn_matrix.each do |el|
        key =  "show_#{el[1].to_s}" 
        puts el[0].send(Sequin::Membership.alert_colors[el[1]][:present]) unless options[key].nil? && options["show_all"].nil?
      end
      
    end

    private

    def self.alert_colors
      {
        :found => {:present => "green", :absent => "red"} , 
        :missing => {:present =>"red",  :absent => "green"} , 
        :extra => {:present =>"yellow",  :absent => "green"}
      }
    end

  end

end