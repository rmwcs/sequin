# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "sequin/version"

Gem::Specification.new do |s|
  s.name        = "sequin"
  s.version     = Sequin::VERSION
  s.platform    = Gem::Platform::RUBY
  s.authors     = ["Julian Mann"]
  s.email       = ["julian.mann@gmail.com"]
  s.homepage    = ""
  s.summary     = %q{Tools to deal with file sequences.}
  s.description = %q{Sequin is a collection of commands to list, test and modify sequences of files on disk.}
  s.add_dependency('thor')
  s.rubyforge_project = "sequin"

  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]
end
