

Sequin
======

Description
--------

Sequin is a collection of thor tasks that allow you to list, check names, and modify sequences of files on disk.

Installation
--------

Update rubyGems:

	$ sudo gem update --system

Install sequin

    $ sudo gem install sequin

Thor will also be installed if it is not currently on your system.

Usage
--------

The command is seq, followed by a task:

Tasks

*  seq list [FILES]                     # List files as sequences.
*  seq match [FILES] -r, --regex=REGEX  # Match filename naming conventions using a regular expression.
*  seq check [SIGNATURE]                # Check the existence of files defined by a sequence signature.
*  seq generate [NAME]                  # Generate a seq config file containing regular expression definitions used in the seq match task.
*  seq help [TASK]                      # Describe available tasks or one specific task.
*  seq usage                            # Print the usage banner.

All tasks have a single letter alias. Use m instead of match, c instead of check etc.


### List Sequences

    seq list [FILES]

List the given files as sequences where possible. 

Options: 

* -v, [--verbose=N] Specify how verbose to be when listing missing frames.

  * 0 Dont mention missing frames
  * 1 show missing frame count
  * 2 list missing frame numbers
  * Default: 1

Each line of output from this task is a **sequence signature.** It consists of a **filename template**: a filename where the sequence digits replaced with "#"s, followed by a **range spec**: start, end, and by frames in square brackets. 

For example: `../test/a/op_063_8900.####.jpg [0 to 25 by 1]`



If any frames are missing, the range spec will be colored red in the output, as will other information about the missing frames according to the verbose options.

    $ seq l * -v 2
    h264_960x540_film.mov
    mjpega_1920x1080.mov
    #### [1 to 30 by 1]
    006_057_s3d_lf_r04.####.dpx [1 to 30 by 1]
    006_057_s3d_lf_r05.#.dpx [1 to 30 by 1] - missing: [ 11,14,15,18,22 ]
    006_057_s3d_lf_r06.#.dpx [1 to 30 by 1]
    008_055_s3d_lf_r04.####.dpx [1 to 50 by 1]
    fn_by_three.#.jpg [1 to 25 by 3] - missing: [ 19 ]
    fn_by_two.#.jpg [1 to 29 by 2]
    
    $ seq l 006* -v 1
    006_057_s3d_lf_r04.####.dpx [1 to 30 by 1]
    006_057_s3d_lf_r05.#.dpx [1 to 30 by 1] - missing: [ 5 frames ]
    006_057_s3d_lf_r06.#.dpx [1 to 30 by 1]


### Match

    seq match [FILES] -r, --regex=REGEX

Make sure filenames conform to file naming conventions by testing them against a regular expression.

Options:

*  -r, --regex=REGEX     # Specify a regular expression literally, or by reference to one listed in ~/.seq.yml
*  -m, [--show-matched]  # List files that match the regex, in green
*  -f, [--show-failed]   # List files that failed to match the regex, in red
*  -a, [--show-all]      # List all the files in order with color coding.

Normally you wouldn't want to type lengthy regular expressions on the command line, so instead they can be stored in a yaml file and referred to by the keys in the yaml data structure. The keys would typically represent projects and filetypes. 

The yaml file looks something like this:

        potter: 
          dpx: ^[0-9]{3}_[0-9]{3}_s3d_(rt|lf)_r[0-9]{2}\.[0-9]{4}\.dpx$
          mcx: ^[0-9]{3}_[0-9]{3}_ver[0-9]{2,3}\.[0-9]{4}\.xml$
        conan: 
          dpx: ^[a-z]{2}_[0-9]{3}_[0-9]{4}_(L|R|C)\.[0-9]{4,5}\.dpx$
          mov: ^[a-z]{2}_h264_[0-9]{3,4}x[0-9]{3,4}_(film|pal|ntsc)\.mov$

The regular expression can then be referred to on the command line with `--regex potter:dpx` and so on.

    $ seq m * -r potter:dpx
    Testing against regular expression: /^[0-9]{3}_[0-9]{3}_s3d_(rt|lf)_r[0-9]{2}\.[0-9]{4}\.dpx$/
    h264_960x540_film.mov FAILED
    mjpega_1920x1080.mov FAILED
    #### [1 to 30 by 1] (0 MATCHED, 30 FAILED)
    006_057_s3d_lf_r04.####.dpx [1 to 30 by 1] (30 MATCHED)
    006_057_s3d_lf_r05.#.dpx [1 to 30 by 1] (0 MATCHED, 25 FAILED)
    006_057_s3d_lf_r06.#.dpx [1 to 30 by 1] (0 MATCHED, 30 FAILED)
    008_055_s3d_lf_r04.####.dpx [1 to 50 by 1] (50 MATCHED)
    fn_by_three.#.jpg [1 to 28 by 3] (0 MATCHED, 10 FAILED)
    fn_by_two.#.jpg [1 to 29 by 2] (0 MATCHED, 15 FAILED)
    op_063_8900.####.jpg [0 to 25 by 1] (0 MATCHED, 26 FAILED)




### Check membership

    seq check [SIGNATURE]

Check the existence of files defined by a sequence signature. 

Options:

* -f, [--show-found]    # List files from the signature that are on disk.
* -m, [--show-missing]  # List files from the signature that are missing on disk.
* -e, [--show-extra]    # List files that are on disk but not in the signature.
* -a, [--show-all]      # List the union of all files on disk or in the signature.
    

Files may either be found, missing, or extra. Extra means the file exists on disk but not in the signature. With no flags, the output will be just a single line showing the number of found, missing or extra. By using the flags, you can list the individual files in the sequence with color coding and and in order. 

    $ seq c 006_057_s3d_lf_r04.####.dpx [1 to 30 by 1]
    006_057_s3d_lf_r04.####.dpx [found: 30] [missing: 0] [extra: 0]
    
    $  seq c 006_057_s3d_lf_r04.####.dpx [5 to 35 by 1]
    006_057_s3d_lf_r04.####.dpx [found: 26] [missing: 5] [extra: 4]
    
	$  seq c 006_057_s3d_lf_r04.####.dpx [5 to 35 by 2] -m
	006_057_s3d_lf_r04.####.dpx [found: 13] [missing: 3] [extra: 17]
	006_057_s3d_lf_r04.0031.dpx
	006_057_s3d_lf_r04.0033.dpx
	006_057_s3d_lf_r04.0035.dpx

### Generate

    seq generate [NAME]

Generate the .sequin config file in your home directory ~/.seq.yml

Options:
* -f, [--force]  # Overwrite the file if it exists.

Currently, NAME must be config. This file contains regular expression definitions used in the `seq match` task.

    $  seq g config
    Skipping write of /Users/julianmann/.seq.yml because it exists. Use --force to remove it.

    $  seq g config -f
    Generating /Users/julianmann/.seq.yml

How Sequin finds sequences
-------

The goal of the sequence listing algorithm is to find sequences without the need
for the user to specify where the numbers are in the filenames, or what characters
delimit the numbers. This can be tricky because a filename may contain many parts 
containing numbers. Here's a real world example: `004_055_s3d_lf_r04.0001.dpx` 
How can we know which number part correctly represents the sequence? 
It could be any of the following

    ###_055_s3d_lf_r04.0001.dpx
    004_###_s3d_lf_r04.0001.dpx
    004_055_s#d_lf_r04.0001.dpx
    004_055_s3d_lf_r##.0001.dpx
    004_055_s3d_lf_r04.####.dpx

The strategy we employ is to use the number part that causes the file to be in the largest sequence.

Consider the files:

    file.04.0001.ext
    file.04.0002.ext
    file.04.0003.ext
    file.05.0001.ext
    file.05.0002.ext
    file.05.0003.ext

There are 5 possible sequences here. Each file could be in 2 of them, one containing 2 files, one containing 3.

    file.04.0001.ext could be in file.##.0001.ext or file.04.####.ext
    
There are more files in `file.04.####.ext` than `file.##.0001.ext`, so we resolve to `file.04.####.ext` for this file.


To Do
-------

* Add support for listing recursively 
* Add modifier tasks such as pad, offset, replace etc.
* Test colors in shells other than bash.
* Test on Ruby 1.8.7. Currently works under Ruby 1.9.2


License
-------

See the LICENSE file for further details. 
